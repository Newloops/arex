export interface Deal {
  id: string;
  name: string;
  address: string;
  price: string;
  type: string;
  dueDate: string;
}