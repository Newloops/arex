import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  showModalFavorites() {
    const dialogRef = this.dialog.open(ModalFavorites, {
      width: '850px',
      autoFocus: false,
      maxHeight: '90vh',
      data: JSON.parse(localStorage.getItem('favorites'))
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

}

@Component({
  selector: 'modal-favorites',
  templateUrl: '../../modals/modal-favorites.html',
})
export class ModalFavorites {

  public filter: any;
  public dealTypes: any[] = ['acquisition','development','financing','leasing'];

  constructor(
    public dialogRef: MatDialogRef<ModalFavorites>,
    @Inject(MAT_DIALOG_DATA) public data
  ){
    this.filter = {
      name: '',
      type: ''
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  searchFilter(filter) {

    let favorites = JSON.parse(localStorage.getItem('favorites'));

    this.data = favorites.filter((value) => {
      return value.name.toLowerCase().indexOf(filter.name.toLowerCase()) > -1 &&
             value.type.toLowerCase().indexOf(filter.type.toLowerCase()) > -1
    })

    console.log(this.data);
  }

  deleteFavorites(item) {
    let favorites = JSON.parse(localStorage.getItem('favorites'));
    favorites.forEach((value, index) => {
      if(favorites[index].id === item.id) {
        favorites.splice(index, 1);
        localStorage.setItem('favorites', JSON.stringify(favorites));
        return false;
      }
    });
    this.data = JSON.parse(localStorage.getItem('favorites'));
  }

}
