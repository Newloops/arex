import { Component, OnInit } from '@angular/core';
import deals from 'src/assets/json/deals.json';
import { Deal } from 'src/app/interfaces/deal';
import * as moment from 'moment';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  public deals: Deal[] = deals;
  public dealTypes: any[] = ['acquisition','development','financing','leasing'];
  public filter: any;
  public result: any[] = deals;
  public orderPriceToggle: boolean = true;
  public orderTypeToggle: boolean = true;
  public orderDateToggle: boolean = true;
  public favorites: any[] = [];

  constructor() {
    this.filter = {
      name: '',
      address: '',
      price: '',
      type: '',
      dueDate: null
    }
  }

  ngOnInit() {
  }

  searchFilter(filter) {
    
    let dueDateToUnix = filter.dueDate !== null ? moment(filter.dueDate).unix() : 0;

    this.result = this.deals.filter((value) => {
      let dueDate = moment(value.dueDate).unix();
      return value.name.toLowerCase().indexOf(filter.name.toLowerCase()) > -1 &&
             value.address.toLowerCase().indexOf(filter.address.toLowerCase()) > -1 &&
             value.price.toLowerCase().indexOf(filter.price.toLowerCase()) > -1 &&
             value.type.toLowerCase().indexOf(filter.type.toLowerCase()) > -1 &&
             (dueDateToUnix === 0 || dueDate <= dueDateToUnix)
    })

    console.log(this.result);
  }

  sortPrice() {
    if(this.orderPriceToggle) {
      this.result = this.result.sort(function(a, b){
          return a.price - b.price;
      });
      this.orderPriceToggle = false;
    } else {
      this.result = this.result.sort(function(a, b){
        return  b.price - a.price;
      });
      this.orderPriceToggle = true;
    }
  }

  sortType() {
    if(this.orderTypeToggle) {
      this.result = this.result.sort(function(a, b){
          return a.type < b.type ? -1 : a.type > b.type ? 1 : 0;
      });
      this.orderTypeToggle = false;
    } else {
      this.result = this.result.sort(function(a, b){
        return  a.type > b.type ? -1 : a.type < b.type ? 1 : 0;
      });
      this.orderTypeToggle = true;
    }
  }

  sortDate() {
    if(this.orderDateToggle) {
      this.result = this.result.sort(function(a, b){
          return moment(a.dueDate).unix() - moment(b.dueDate).unix();
      });
      this.orderDateToggle = false;
    } else {
      this.result = this.result.sort(function(a, b){
        return  moment(b.dueDate).unix() - moment(a.dueDate).unix();
      });
      this.orderDateToggle = true;
    }
  }

  addFavorites(data) {
    console.log(JSON.parse(localStorage.getItem('favorites')));
    if(JSON.parse(localStorage.getItem('favorites')) === null) {
      this.favorites.push(data);
      localStorage.setItem('favorites', JSON.stringify(this.favorites));
    } else {
      let favorites = JSON.parse(localStorage.getItem('favorites'));
      if(!favorites.some(item => item.id === data.id)) {
        favorites.push(data);
        localStorage.setItem('favorites', JSON.stringify(favorites));
      }
    }
    /* this.favorites.push(data);
    localStorage.setItem('favorites', JSON.stringify(this.favorites)); */
  }

}
